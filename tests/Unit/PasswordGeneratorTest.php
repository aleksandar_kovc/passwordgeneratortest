<?php

namespace Tests\Unit;

use App\Services\GeneratePasswordService;
use PHPUnit\Framework\TestCase;

class PasswordGeneratorTest extends TestCase
{
    /**
     * Testing password length.
     *
     * @return void
     */
    public function test_length(): void
    {
        $length = mt_rand(8, 120);

        $service = new GeneratePasswordService(3, $length);
        $password = $service->generate();

        $this->assertEquals($length, strlen($password));
    }

    /**
     * Testing password minimum length.
     *
     * @return void
     */
    public function test_minimum_length(): void
    {
        $length = mt_rand(1, GeneratePasswordService::MINIMAL_LENGTH);

        $service = new GeneratePasswordService(3, $length);
        $password = $service->generate();

        $this->assertEquals(GeneratePasswordService::MINIMAL_LENGTH, strlen($password));
    }

    /**
     * Testing password one strength.
     *
     * @return void
     */
    public function test_strength_one_password(): void
    {
        $length = mt_rand(10, 100);

        $service = new GeneratePasswordService(1, $length);
        $password = $service->generate();

        $uppercaseCount = 0;
        $lowercaseCount = 0;

        $explodedPassword = str_split($password);

        $lowercaseLetters = [];

        foreach (GeneratePasswordService::LETTERS as $letter) {
            $lowercaseLetters[] = strtolower($letter);
        }

        foreach ($explodedPassword as $char) {
            if (in_array($char, GeneratePasswordService::LETTERS)) {
                $uppercaseCount++;
            } elseif (in_array($char, $lowercaseLetters)) {
                $lowercaseCount++;
            }
        }

        $this->assertGreaterThanOrEqual(2, $uppercaseCount);
        $this->assertGreaterThanOrEqual(1, $lowercaseCount);
        $this->assertEquals($length, $uppercaseCount + $lowercaseCount);
    }

    /**
     * Testing password two strength.
     *
     * @return void
     */
    public function test_strength_two_password(): void
    {
        $length = mt_rand(10, 100);

        $service = new GeneratePasswordService(2, $length);
        $password = $service->generate();

        $uppercaseCount = 0;
        $lowercaseCount = 0;
        $numberCount = 0;

        $explodedPassword = str_split($password);

        $lowercaseLetters = [];

        foreach (GeneratePasswordService::LETTERS as $letter) {
            $lowercaseLetters[] = strtolower($letter);
        }

        foreach ($explodedPassword as $char) {
            if (in_array($char, GeneratePasswordService::LETTERS)) {
                $uppercaseCount++;
            } elseif (in_array($char, $lowercaseLetters)) {
                $lowercaseCount++;
            } elseif (in_array($char, [2, 3, 4, 5])) {
                $numberCount++;
            }
        }

        $this->assertGreaterThanOrEqual(2, $uppercaseCount);
        $this->assertGreaterThanOrEqual(1, $lowercaseCount);
        $this->assertGreaterThanOrEqual(1, $numberCount);
        $this->assertEquals($length, $uppercaseCount + $lowercaseCount + $numberCount);
    }

    /**
     * Testing password three strength.
     *
     * @return void
     */
    public function test_strength_three_password(): void
    {
        $length = mt_rand(10, 100);

        $service = new GeneratePasswordService(3, $length);
        $password = $service->generate();

        $uppercaseCount = 0;
        $lowercaseCount = 0;
        $numberCount = 0;
        $specialCharsCount = 0;

        $explodedPassword = str_split($password);

        $lowercaseLetters = [];

        foreach (GeneratePasswordService::LETTERS as $letter) {
            $lowercaseLetters[] = strtolower($letter);
        }

        foreach ($explodedPassword as $char) {
            if (in_array($char, GeneratePasswordService::LETTERS)) {
                $uppercaseCount++;
            } elseif (in_array($char, $lowercaseLetters)) {
                $lowercaseCount++;
            } elseif (in_array($char, [2, 3, 4, 5])) {
                $numberCount++;
            } elseif (in_array($char, GeneratePasswordService::SPECIAL_CHARS)) {
                $specialCharsCount++;
            }
        }

        $this->assertGreaterThanOrEqual(2, $uppercaseCount);
        $this->assertGreaterThanOrEqual(1, $lowercaseCount);
        $this->assertGreaterThanOrEqual(1, $numberCount);
        $this->assertGreaterThanOrEqual(1, $specialCharsCount);
        $this->assertEquals($length, $uppercaseCount + $lowercaseCount + $numberCount + $specialCharsCount);
    }
}
