<?php

namespace App\Services;

use Illuminate\Support\Arr;

class GeneratePasswordService
{
    const LETTERS = [
        'Q', 'W','E','R', 'T', 'Y', 'U', 'I',
        'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H',
        'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',
    ];

    const SPECIAL_CHARS = ['!', '#', '$', '%', '&', '(', ')', '{', '}', '[', ']', '='];

    const MINIMAL_LENGTH = 6;

    /**
     * @var int
     */
    private $strength;

    /**
     * @var int
     */
    private $length;

    /**
     * @param int $strength Password strength.
     * @param int $length Password length.
     */
    public function __construct(int $strength, int $length)
    {
        $this->strength = !in_array($strength, [1, 2, 3]) ? 1 : $strength;
        $this->length = $length < self::MINIMAL_LENGTH ? self::MINIMAL_LENGTH : $length;
    }

    /**
     * Generates password.
     *
     * @return string
     */
    public function generate(): string
    {
        switch ($this->strength) {
            case 1:
                $characters = $this->generateStrengthOnePassword();
                break;
            case 2:
                $characters = $this->generateStrengthTwoPassword();
                break;
            case 3:
                $characters = $this->generateStrengthThreePassword();
                break;
        }

        return implode('', Arr::shuffle($characters));
    }

    /**
     * Generates weakest password.
     *
     * @return array
     */
    private function generateStrengthOnePassword(): array
    {
        $password = [];
        $iterations = ceil($this->length / 3);

        for ($i = 0; $i < $iterations; $i++) {
            $password = array_merge($password, $this->twoUppercaseLetters());
            $password = array_merge($password, $this->oneLowercaseLetter());
        }

        return array_slice($password, 0, $this->length);
    }

    /**
     * Generates password with medium strength.
     *
     * @return array
     */
    private function generateStrengthTwoPassword(): array
    {
        $password = [];
        $iterations = ceil($this->length / 4);

        for ($i = 0; $i < $iterations; $i++) {
            $password = array_merge($password, $this->twoUppercaseLetters());
            $password = array_merge($password, $this->oneLowercaseLetter());
            $password = array_merge($password, $this->oneNumber());
        }

        return array_slice($password, 0, $this->length);
    }

    /**
     * Generates strongest password.
     *
     * @return array
     */
    private function generateStrengthThreePassword(): array
    {
        $password = [];
        $iterations = ceil($this->length / 5);

        for ($i = 0; $i < $iterations; $i++) {
            $password = array_merge($password, $this->twoUppercaseLetters());
            $password = array_merge($password, $this->oneLowercaseLetter());
            $password = array_merge($password, $this->oneNumber());
            $password = array_merge($password, $this->oneSpecialChar());
        }

        return array_slice($password, 0, $this->length);
    }

    /**
     * Generates two uppercase letters.
     *
     * @return array
     */
    private function twoUppercaseLetters(): array
    {
        $lettersCount = count(self::LETTERS) - 1;

        return [
            self::LETTERS[mt_rand(0, $lettersCount)],
            self::LETTERS[mt_rand(0, $lettersCount)],
        ];
    }

    /**
     * Generates one lowercase letter.
     *
     * @return array
     */
    private function oneLowercaseLetter(): array
    {
        return [strtolower(self::LETTERS[mt_rand(0, count(self::LETTERS) - 1)])];
    }

    /**
     * Generates one number from 2 to 5.
     *
     * @return array
     */
    private function oneNumber(): array
    {
        return [mt_rand(2, 5)];
    }

    /**
     * Generates one special character.
     *
     * @return array
     */
    private function oneSpecialChar(): array
    {
        return [self::SPECIAL_CHARS[mt_rand(0, count(self::SPECIAL_CHARS) - 1)]];
    }
}
